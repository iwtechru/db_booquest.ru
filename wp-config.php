<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'newbooquest');

/** Имя пользователя MySQL */
define('DB_USER', 'newbooquest');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'E17H71o4m85a57g2');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z#M,T-2v[|:nS04)pOL?UQ{z.mbNI>lb%eNH<Ni4~Y+-ln[[@saX:&EI[a.D-9Dq');
define('SECURE_AUTH_KEY',  's8Dgx=:l3+/pgr+mz+|1{|y!nP,/<mFc)?Bn@P0$+|I.r><Kv<,$M7?;N8Qy6#|1');
define('LOGGED_IN_KEY',    'aNl<L2uzds5_HBQH|Nc2Sk!nwQF)vV%iu&k0Y]*1W!7};(}Xb9*%AV.PeyCZP$f)');
define('NONCE_KEY',        'DFzBC1d8TwY0 /|+q{+3;`F0FX]cE{I |ws>pqXU_;B,i)w_5tK0#0:H!hG:j%S:');
define('AUTH_SALT',        '?N9sd>D(K0,^B=Qo5hvDN8*ncqGz~Jsyhv7 l7|Zh6dAn|3Xje@nz%rIet`rFQl2');
define('SECURE_AUTH_SALT', 'S@M|GD}E i~~r}?d,!N;cYu,3qqLQOj+LWj+?I[Cme7{S?I`>Hrd`j?)Q::kd#I]');
define('LOGGED_IN_SALT',   '+SU&kHYiCI%XSucxA;;x4Y#73xF)xuP#|n<<kH/2?_ov:eh&1}**zW>`#2$kYVku');
define('NONCE_SALT',       '^3*P7L;v6HuOVMm%}VGG=#RU` 1>R+,>CdO+KBNBDcI!!n^Dmo0BC; _*T(nz*g}');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define(‘WP_MEMORY_LIMIT’, ’64MB’);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
